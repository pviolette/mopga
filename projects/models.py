from django.db import models


# Create your models here.


class Author(models.Model):
    score = models.IntegerField


class Project(models.Model):
    name = models.CharField(max_length=64)
    short_desc = models.CharField(max_length=255)
    long_desc = models.TextField()
    budget = models.DecimalField(max_digits=11, decimal_places=2)  # Jusqu'à 1 milliard avec 2 chiffre après la virgule
    author = models.ForeignKey(Author)
